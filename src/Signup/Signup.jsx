import React, { useState } from "react";

const Signup = () => {
  const handleSubmitForm = (e) => {
    e.preventDefault();
    const name = e.target[0].value;
    const email = e.target[1].value;
    const phone = e.target[2].value;
    const file = e.target[3].value;
    const address = e.target[4].value;
    const gender = e;
    const subject = e.target[6].value;
    const image = e.target[7].value;
    console.log(gender);
  };

  const [formData, setFormData] = useState({
    textInput: "",
    passwordInput: "",
    numberInput: 0,
    checkboxInput: false,
    radioInput: "",
    selectInput: "option1",
    textareaInput: "",
  });

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;

    setFormData((prevData) => ({
      ...prevData,
      [name]: type === "checkbox" ? checked : value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Form Data:", formData);
    // Add logic for form submission or API call here
  };

  return (
    <div className="flex flex-col items-center h-screen">
      <h1 className="text-3xl font-bold mb-6">register/Sign Up</h1>

      {/* <form onSubmit={(e) => handleSubmitForm(e)}>
        <div className="mb-4">
          <label className="block text-sm font-medium">Name</label>
          <input
            type="text"
            placeholder="Enter Name"
            name="name"
            className="mt-1 p-2 border rounded w-full"
          />
        </div>

        <div className="mb-4">
          <label className="block">Email</label>
          <input
            type="email"
            placeholder="Enter Email"
            name="email"
            className="mt-1 p-2 border w-full"
          />
        </div>

        <div className="mb-4">
          <label className="block">Phone</label>
          <input
            type="tel"
            placeholder="Enter Phone"
            className="mt-1 p-2 border w-full"
          />
        </div>

        <div className="mb-4">
          <label htmlFor="address" className="block font-medium">
            Address
          </label>
          <textarea
            name="address"
            id=""
            cols=""
            rows="3"
            placeholder="Enter Address"
            className="mt-1 p-2 border w-full"
          ></textarea>
        </div>

        <div className="mb-3 flex">
          <label htmlFor="">Gender : </label>
          <input
            type="radio"
            name="Gender"
            value="male"
            className="mx-1"
          />{" "}
          Male
          <input
            type="radio"
            name="Gender"
            value="female"
            className="mx-2"
          />{" "}
          Female
          <input
            type="radio"
            name="Gender"
            value="other"
            className="mx-1"
          />{" "}
          Other
        </div>

        <div className="mb-4 flex">
          <label htmlFor="">Subject : </label>
          <input type="checkbox" name="Bangla" className="mx-1" /> Bangla
          <input type="checkbox" name="English" className="mx-1" /> English
          <input type="checkbox" name="Arabic" className="mx-1" /> Arabic
          <input type="checkbox" name="Other" className="mx-1" /> Other
        </div>

        <div className="mb-4">
          <label htmlFor="">Image</label>
          <input type="file" accept="" />
        </div>

        <div>
          <label></label>
          <input
            type="submit"
            value="Register"
            className="bg-green-500 text-white p-4 rounded w-full shadow-xl"
          />
        </div>
      </form> */}

      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="textInput">Text Input:</label>
          <input
            type="text"
            id="textInput"
            name="textInput"
            value={formData.textInput}
            onChange={handleChange}
          />
        </div>

        <div>
          <label htmlFor="passwordInput">Password Input:</label>
          <input
            type="password"
            id="passwordInput"
            name="passwordInput"
            value={formData.passwordInput}
            onChange={handleChange}
          />
        </div>

        <div>
          <label htmlFor="numberInput">Number Input:</label>
          <input
            type="number"
            id="numberInput"
            name="numberInput"
            value={formData.numberInput}
            onChange={handleChange}
          />
        </div>

        <div>
          <label>
            <input
              type="checkbox"
              name="checkboxInput"
              checked={formData.checkboxInput}
              onChange={handleChange}
            />
            Checkbox Input
          </label>
        </div>

        <div>
          <label>
            <input
              type="radio"
              name="radioInput"
              value="option1"
              checked={formData.radioInput === "option1"}
              onChange={handleChange}
            />
            Radio Option 1
          </label>
          <label>
            <input
              type="radio"
              name="radioInput"
              value="option2"
              checked={formData.radioInput === "option2"}
              onChange={handleChange}
            />
            Radio Option 2
          </label>
        </div>

        <div>
          <label htmlFor="selectInput">Select Input:</label>
          <select
            id="selectInput"
            name="selectInput"
            value={formData.selectInput}
            onChange={handleChange}
          >
            <option value="option1">Option 1</option>
            <option value="option2">Option 2</option>
            <option value="option3">Option 3</option>
          </select>
        </div>

        <div>
          <label htmlFor="textareaInput">Textarea Input:</label>
          <textarea
            id="textareaInput"
            name="textareaInput"
            value={formData.textareaInput}
            onChange={handleChange}
          />
        </div>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Signup;
